<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'apsulis_vide_description' => 'Ce plugin a la même vocation que zpip-vide ou spipr-vide, mais pour la structure Z-core utilisé par Apsulis. Les squelettes non utilisés par le site, mais disponibles dans squelettes-dist sont bloqués pour ne pas renvoyer de contenu.',
	'apsulis_vide_nom' => 'Apsulis Vide',
	'apsulis_vide_slogan' => 'Bloque certains squelettes par défaut.',
);
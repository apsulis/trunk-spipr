<?php

// Notation dans les articles des corrections éditoriales ultérieures à apporter

// À corriger
if (!defined('_AD_A_FIXER')) {
	define('_AD_A_FIXER', '@@FIXME');
}

// À faire
if (!defined('_AD_A_FAIRE')) {
	define('_AD_A_FAIRE', '@@TODO');
}

// Notes
if (!defined('_AD_NOTE')) {
	define('_AD_NOTE', '@@NOTE');
}

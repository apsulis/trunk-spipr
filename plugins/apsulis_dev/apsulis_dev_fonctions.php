<?php
/**
 * Fonctions utiles au plugin Apsulis Développement
 *
 * @plugin     Apsulis Développement
 * @copyright  2015
 * @author     Apsulis
 * @licence    GNU/GPL
 * @package    SPIP\Apsulis_dev\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


function defaut_tri_defined_dev($defaut){
	if (!defined('_TRI_ARTICLES_RUBRIQUE'))
		return $defaut;

	$sens = 1;
	$tri = trim(_TRI_ARTICLES_RUBRIQUE);
	$tri = explode(" ",$tri);
	if (strncasecmp(end($tri),"DESC",4)==0){
		$sens = -1;
		array_pop($tri);
	}
	$tri = implode(' ',$tri);
	$tri = array($tri => $sens);
	foreach($defaut as $n => $s){
		if (!isset($tri[$n])) {
			$tri[$n] = $s;
		}
	}
	return $tri;
}

function defaut_tri_par_dev($par, $defaut){
	if (!defined('_TRI_ARTICLES_RUBRIQUE')) {
		return $par;
	}
	$par = array_keys($defaut);
	return reset($par);
}

<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'afficher_tous' => 'Tous',
	'afficher_a_fixer' => 'À corriger',
	'afficher_a_faire' => 'À faire',
	'afficher_notes' => 'Notes',
	'afficher_tous' => 'Tous',

	// M
	'maintenance_editoriale' => 'Maintenance éditoriale',
);

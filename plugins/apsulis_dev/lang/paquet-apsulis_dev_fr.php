<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'apsulis_dev_description' => 'Aides pour le développement éditorial du site à l\'aide des syntaxes @@note, @@fixme, @@todo utilisables dans le texte des articles et que l\'on peut retourver sur la page de « maintenance éditoriale »',
	'apsulis_dev_nom' => 'Apsulis Développement',
	'apsulis_dev_slogan' => 'Ajoute les syntaxes @@note, @@fixme, @@todo',
);

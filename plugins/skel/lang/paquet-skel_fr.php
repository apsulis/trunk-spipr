<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'skel_description' => 'Squelettes SPIP spécifiques à ce site.',
	'skel_nom' => 'Skel',
	'skel_slogan' => 'Squelettes SPIP du site',
);
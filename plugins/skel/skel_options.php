<?php
/**
 * Options au chargement du plugin Skel
 *
 * @plugin     Skel
 * @copyright  2015
 * @author     Apsulis
 * @licence    GNU/GPL
 * @package    SPIP\Skel\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

if (!isset($GLOBALS['z_blocs'])) {
	/**
	 * Déclaration des blocs utilisés sur ce site.
	**/
	$GLOBALS['z_blocs'] = array(
		'content', // contenu principal. Absence de contenu dedans = 404.
		'aside',
		'extra',
		'head',
		'head_js',
		'header',
		'breadcrumb',
		'footer',
	);
}

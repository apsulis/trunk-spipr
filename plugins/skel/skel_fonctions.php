<?php
/**
 * Fonctions utiles au plugin Skel
 *
 * @plugin     Skel
 * @copyright  2015
 * @author     Apsulis
 * @licence    GNU/GPL
 * @package    SPIP\Skel\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('public/skel_filtres_dates');

/**
 * Recadre une image pour quelle rentre entièrement  les dimensions indiquées
 *
 * Cela peut rogner un peu des morceaux.
 * @param string $p L'image (balise html) ou son chemin
 * @param int $width Largeur
 * @param int $height Hauteur
 * @param string $pos_recadre Type de recadrage.
 * @return string Balise html de l'image.
**/
function image_remplissage($p, $width = 800, $height= 600, $pos_recadre = 'focus') {
	$p = filtrer('image_recadre', $p, "$width:$height", "-", $pos_recadre);
	$p = filtrer('image_reduire', $p, $width, $height);
	return $p;
}

/**
 * Recadre une image pour quelle ne dépasse pas les dimensions indiquées
 *
 * @param string $p L'image (balise html) ou son chemin
 * @param int $width Largeur
 * @param int $height Hauteur
 * @return string Balise html de l'image.
**/
function image_entiere($p, $width = 800, $height= 600) {
	$p = filtrer('image_reduire', $p, $width, $height);
	return $p;
}

/**
 * un filtre pour transformer les URLs relatives en URLs absolues ;
 * ne s'applique qu'aux textes contenant des liens
 *
 * idem le filtre liens_absolus du core mais ne touche pas aux urls commencant par @@ qui sont en fait des variables
 *
 * @param string $texte
 * @param string $base
 * @return string
 */
function skel_newsletters_liens_absolus($texte, $base='') {
	if (!$base) {
		$base = url_de_base() . (_DIR_RACINE ? _DIR_RESTREINT_ABS : '');
		// respecter le protocole http/https de l'adresse principale du site
		// car le back-office peut etre en https, mais le site public en http
		$protocole = explode("://",$GLOBALS['meta']['adresse_site']);
		$protocole = reset($protocole) . ":";
		$base = $protocole . protocole_implicite($base);
	}

	if (preg_match_all(',(<(a|link|image)[[:space:]]+[^<>]*>),imsS',$texte, $liens, PREG_SET_ORDER)) {
		foreach ($liens as $lien) {
			$href = extraire_attribut($lien[0],"href");
			if ($href AND strncmp($href,'#',1)!==0 AND strncmp($href,'@',1)!==0){
				$abs = url_absolue($href, $base);
				if ($abs <> $href){
					$href = str_replace($href,$abs,$lien[0]);
					$texte = str_replace($lien[0], $href, $texte);
				}
			}
		}
	}
	if (preg_match_all(',(<(img|script)[[:space:]]+[^<>]*>),imsS',$texte, $liens, PREG_SET_ORDER)) {
		foreach ($liens as $lien) {
			if ($src = extraire_attribut($lien[0],"src")){
				$abs = url_absolue($src, $base);
				if ($abs <> $src){
					$src = str_replace($src,$abs,$lien[0]);
					$texte = str_replace($lien[0], $src, $texte);
				}
			}
		}
	}
	return $texte;
}

<?php

function formulaires_accordeons_dist($champs){
	$i = 0;
	foreach ($champs as $key => $value) {
		if (_request($value)) {
			$i ++;
		}
	}

	if ($i == 1) {
		return '<'._request('modele').'|>';
	}else{
		$retour = '<'._request('modele');
		foreach ($champs as $key => $value) {
			if ($value != 'modele') {
				if (_request($value)) {
					$retour .= '|'.$value.'='._request($value);
				}
			}
		}
		$retour .= '>';

		return $retour;
	}
}


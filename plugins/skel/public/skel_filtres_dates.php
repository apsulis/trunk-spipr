<?php
/**
 * Filtres de dates utiles plugin Skel
 *
 * @plugin     Skel
 * @copyright  2015
 * @author     Apsulis
 * @licence    GNU/GPL
 * @package    SPIP\Skel\Filtres
 */

if (!defined('_ECRIRE_INC_VERSION')) return;



/**
 * Afficher de facon textuelle les dates de début et fin en écriture `d/m` ou `d/m/Y`
 * 
 * @param string $date_debut
 * @param string $date_fin
 * @param string $horaire
 * @return string
 *     Texte de la date
 */
function affdate_debut_fin_dmY($date_debut, $date_fin, $horaire = 'oui'){

	$date_debut = strtotime($date_debut);
	$date_fin   = strtotime($date_fin);

	$d = date('Y-m-d', $date_debut);
	$f = date('Y-m-d', $date_fin);

	$h  = ($horaire==='oui' OR $horaire===true);
	$hd = _T('date_fmt_heures_minutes_court', array('h'=> date("H",$date_debut), 'm'=> date("i",$date_debut)));
	$hf = _T('date_fmt_heures_minutes_court', array('h'=> date("H",$date_fin), 'm'=> date("i",$date_fin)));

	// dates le même jour
	if ($d==$f) {
		$s = affdate_dmY_court($d);
		// avec horaires
		if ($h) {
			if ($hd==$hf) {
				// 20/02 à 18h25
				return _T('date_fmt_jour_heure', array('jour' => $s, 'heure' => $hd));
			} else {
				// 20/02 de 18h25 à 20h30
				return _T('date_fmt_jour_heure_debut_fin', array('jour' => $s, 'heure_debut' => $hd, 'heure_fin' => $hf));
			}
		// sans horaire
		} else {
			return $s;
		}
	}

	// dates les mêmes mois et année (jours différents)
	elseif ((date("Y-m", $date_debut)) == date("Y-m", $date_fin)) {
		// du 2/05 au 6/05
		// du 2/05 à 12h00 au 6/05 à 18h30

		$db = affdate_dmY_court($d, date("Y", $date_fin));
		$df = affdate_dmY_court($f);

		if ($h) {
			$db = _T('date_fmt_jour_heure', array('jour' => $db, 'heure' => $hd));
			$df = _T('date_fmt_jour_heure', array('jour' => $df, 'heure' => $hf));
		}

		return _T('date_fmt_periode', array('date_debut' => $db, 'date_fin' => $df));
	}

	// dates sur des mois différents
	else {
		$db = affdate_dmY_court($d, date("Y", $date_fin));
		$df = affdate_dmY_court($f);

		if ($h){
			$db = _T('date_fmt_jour_heure', array('jour' => $db, 'heure' => $hd)); 
			$df = _T('date_fmt_jour_heure', array('jour' => $df, 'heure' => $hf));
		}

		return _T('date_fmt_periode', array('date_debut' => $db,'date_fin' => $df));
	}
}

/**
 * Retourne la date au format `d/m` ou `d/m/Y` en fonction de l'année de référence
 *
 * @param string $numdate
 *     Une écriture de date
 * @param int|null $annee_courante
 *     L'année de comparaison, utilisera l'année en cours si omis.
 * @return string|null
 *     La date calculée
**/
function affdate_dmY_court($numdate, $annee_courante = null) {
	$date_array = recup_date($numdate, false);
	if (!$date_array) {
		return;
	}
	list($annee, $mois, $jour, $heures, $minutes, $secondes) = $date_array;
	$a = (!empty($annee_courante) ? $annee_courante : date('Y'));
	if ($annee == $a) {
		return "$jour/$mois";
	} else {
		return "$jour/$mois/$annee";
	}
}

<?php
/**
 * Pipelines du plugin Skel
 *
 * @plugin     Skel
 * @copyright  2015
 * @author     Apsulis
 * @licence    GNU/GPL
 * @package    SPIP\Skel\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Ajoute des rôles sur les documents
 */
function skel_declarer_tables_objets_sql($tables){

	// rôles actuels (par défaut 'logo' et 'logo_survol' pour tous les objets)
	// si on a besoin d'en supprimer… sinon pas la peine
	$roles_titres = isset($tables['spip_documents']['roles_titres']) ? $tables['spip_documents']['roles_titres'] : array();
	$roles_objets = isset($tables['spip_documents']['roles_objets']) ? $tables['spip_documents']['roles_objets'] : array();

	// nouveaux rôles
/*
	$roles_titres = array_merge($roles_titres, array(
		'visuel_carrousel' => 'Visuel pour le carrousel',
	));

	$roles_objets['*']['choix'][] = 'visuel_carrousel';
*/

	array_set_merge($tables, 'spip_documents', array(
		"roles_titres" => $roles_titres,
		"roles_objets" => $roles_objets
	));

	return $tables;
}

<?php
define('_DUREE_CACHE_DEFAUT', 24*3600*30);

// Passer le cache à 100Mo
// $quota_cache=1000;
// $GLOBALS['quota_cache'] = 100;

// Activer le plugin nospam sur des formulaires supplémentaires
// $GLOBALS['formulaires_no_spam'][] = 'soumission_annuaire';

/*
 * GESTION DES PARAMETRES DE DEVELOPPEMENT
 */
if ((strstr($_SERVER['HTTP_HOST'],'.apsulis.ninja'))
	OR (strstr($_SERVER['HTTP_HOST'],'.local'))
) {
	define('_NO_CACHE', 1);

	// Activer tous les logs
	define('_LOG_FILTRE_GRAVITE', 8);

	// Activer un Debug Verbose sur SPIP
	error_reporting(E_ALL ^ E_NOTICE);
	ini_set ("display_errors", "On");
	define('SPIP_ERREUR_REPORT', E_ALL ^ E_NOTICE);

	ini_set('xdebug.max_nesting_level', 200);

	/* Activer le débug SQL pour optimiser le code */
	// define('_DEBUG_SLOW_QUERIES', true);
	// define('_BOUCLE_PROFILER', 500);
	// define('_LOG_FILELINE', true);
} else {
	// Serveur de production, on force le port
	// $_SERVER['SERVER_PORT']='443';
}

